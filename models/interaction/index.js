/* jshint esversion: 6, strict: true */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var IteractionSchema = new Schema({
	'kind': {
		'type': String,
		'enum': ['audio', 'file', 'text', 'video'],
	},
	'direction':  {
		'type': String,
		'enum': ['in', 'out']
	},
	'involved': {
		'type': [Schema.Types.ObjectId],
		'ref': 'User'
	},
	'starDate': Number,//meant for a Date.now()
	'end': Number//meant for a Date.now()-> only applicable for calls an such
});

module.exports = mongoose.model('Iteraction', IteractionSchema);