var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

AwayTeamSchema = new Schema({
	'name': String,
	'motto': String,
	'captains': [{
		'type': Schema.Types.ObjectId,
		'ref': 'User'
	}],
	'crew': [{
		 'type': Schema.Types.ObjectId,
		 'ref': 'User'
	}]
});