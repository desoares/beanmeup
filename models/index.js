var mongoose = require('mongoose'),
	config = require('../config'),
	options = {},
	db = config.database.uri;

mongoose.connect(db, options, function (err) {
	if (err) {
		console.log('Error while connecting to the DB' + db + '.');
		return;
	}

	console.log('MongoDB is connected on database ' + db + '.');
});

module.exports = {
	'User': require('./user'),
	'Messages': require('./messages'),
	'AwayTeam': require('./awayTeam')
};