/* jshint ecversion: 6, strict: true */
var mongoose = require('mongoose'),
	bcrypt = require('bcrypt'),
	makeName = require('../../lib/make-name'),
	config = require('../../config');
	Schema = mongoose.Schema;

//objets for reuse
var	reqStr = {
		'type': String,
		'required': true
	},
	uniReqStr = {
		'type': String,
		'required': true,
		'index': {
			'unique': true
		}
	},
	uniStr = {
		'type': String,
		'index': {
			'unique': true
		}
	},
	strAddr = {
		'street': String,
		'number': String,
		'neighborhood': String,
		'zipCode': String,
		'state': String,
		'country': String,
		'lat': Number,
		'lon': Number
	};

var UserSchema = new Schema({
	'auth': {
		'name': uniStr,
		'hash': String,
		'password': reqStr
	},
	'email': {
		'main': uniReqStr,
		'secondary': [String]
	},
	'name': {
		'given': reqStr,
		'middle': String,
		'family': reqStr
	},
	'media': {
		'profile': String,
		'cover': String
	},
	'address': {
		'home': strAddr,
		'work': strAddr
	},
	'phones': [{
		'number': String,
		'countryCode': String,
		'areaCode': String
	}],
	'contacts': [{
		'type': Schema.Types.ObjectId,
		'ref': 'User'
	}],
	'awayTeam': [{
		'type': Schema.Types.ObjectId,
		'ref': 'AwayTeam'
	}],
});

UserSchema.pre('save', function (next) {
	'use strict';
	if (this.isNew && !this.auth.name){
		this.auth.name = makeName.family(this);
	}

	return next();
});

UserSchema.pre('save', function (next) {
	'use strict';
	if (!this.isModified('auth.password')) {
		return next();
	}

	var user = this,
		naCl = config.salt,
		pass = user.auth.password;

	bcrypt.genSalt(naCl, function (err, salt) {
		if (err) {
			return nexxt(err);
		}

		bcrypt.hash(pass, salt, function (err, hash) {
			if (err) {
				return  next(err);
			}

			user.auth.password = hash;
			user.auth.hash = salt;

			return next();
		});
	});
});

UserSchema.statics.comparePass = function (pass, dbPass, cb) {
	'use strict';
	bcrypt.compare(pass, dbPass, function (err, match) {
		if (err) {
			return cb(err);
		}

		return cb(null, match);
	});
};

UserSchema.path('email.main').validate(function (email) {
	'use strict';
	var regularExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	return regularExp.test(email);
}, 'INVALID_EMAIL_ERROR');

module.exports = mongoose.model('User', UserSchema);