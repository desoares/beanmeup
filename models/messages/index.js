var mongoose =  require('mongoose'),
	Schema = mongoose.Schema;

MessageSchema = new Schema({
	'sender': Schema.Types.ObjectId,//can only be users
	'receiver': Schema.Types.ObjectId,//can be both users or groups
	'message': {
		'type': String,
		'required': true
	},
	'sent': Date,
	'received': Date
});

