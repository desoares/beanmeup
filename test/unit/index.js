var fs = require('fs'),
	files = fs.readdirSync(__dirname + '/');

files.forEach(function (file) {
	'use strict';

	if (file != 'index.js') {
		var name = file.replace('js', '');

		module.exports[name] = require('./' + file);
	}
});