var expect = require('chai').expect,
	makeName = require('../../lib/make-name'),
	user = {
		'name': {
			'given': 'Teste',
			'middle': 'Unit',
			'family': 'Testerson'
		},
		'email': {
			'main': 'teste@test.com'
		}
	},
	user1 = {
		'name': {
			'given': 'Teste',
			'family': 'Testerson'
		},
		'email': {
			'main': 'test@test.com'
		}
	};

describe('Tests auth name creation based on user objects.', function () {

	it('Should return Teste.Testerson for family method.', function (done) {

		var name = makeName.family(user);

		expect(name).to.deep.equal('Teste.Testerson');
		done();
	});

	it('Should return Teste.Unit.Testerson for full method.', function (done) {
		var name = makeName.full(user);

		expect(name).to.deep.equal('Teste.Unit.Testerson');
		done();
	});

	it('Should return Teste.Testerson for calling full with no middle name.', function (done) {
		var name = makeName.full(user1);

		expect(name).to.deep.equal('Teste.Testerson');
		done();

	});

	it('Should return the inverse of family for reverseFam', function (done) {
		var name = makeName.reverseFam(user);

		expect(name).to.deep.equal('Testerson.Teste');
		done();
	});

	it('Should return the inverse of full for reverseFull', function (done) {
		var name  = makeName.reverseFull(user);

		expect(name).to.deep.equal('Testerson.Unit.Teste');
		done();
	});

	it('Should return the same as reverseFam for calling reverseFull with no middle name', function(done) {
		var name = makeName.reverseFull(user1);

		expect(name).to.deep.equal('Testerson.Teste');
		done();
	});

	it('Should return teste.Testerson for email', function (done) {
		var name = makeName.email(user);

		expect(name).to.deep.equal('teste.Testerson');
		done();
	});

	it('Should return Testerson.teste for reverseMail', function (done) {
		var name = makeName.reverseMail(user);

		expect(name).to.deep.equal('Testerson.teste');
		done();
	});

	it('Should return Teste98 as the next numeric valid user name for increment', function (done) {
		var name  = makeName.increment(user, 'Teste97');

		expect(name).to.deep.equal('Teste98');
		done();
	});

	it('Should return Teste1 as the next numeric valid user name for increment', function (done) {
		var name  = makeName.increment(user, 'Teste');

		expect(name).to.deep.equal('Teste1');
		done();
	});
});

