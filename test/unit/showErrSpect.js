/* jshint esversion: 6, strict: true*/
var showErr = require('../../lib/show-err'),
	expect = require('chai').expect;

describe('Generic app error handler method', ()=>{
	'use strict';

	it('Should return an error object even we do not send one', (done)=>{
		let err = showErr();

		expect(err).to.be.an('object');
		expect(err.message).to.deep.equal('Call to error handles with no arguments.');
		done();
	});
	it('Should return the line of the error ', (done)=>{
		let line = __line,
			file = __file;

		expect(line).to.deep.equal(16);
		expect(file).to.equal(__dirname + '/showErrSpect.js')
		done();
	});
});