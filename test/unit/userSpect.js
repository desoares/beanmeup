/* jshint esversion: 6, strict: true*/
var expect = require('chai').expect,
	mongoose = require('mongoose'),
	fixture = require('mongoose-fixtures'),
	User = require('../../models').User;

describe('User model:', ()=>{
	'use strict';

	before((done)=>{
		fixture.load('../fixtures/users.js', (err)=>{
			if (err) {
				throw err;
			}

			done();
		});
	});
	after((done)=>{
		User.remove((err, deleted)=>{
			if (deleted) {
				done();
			}
		});
	});

	it('Should return a password encrypted', (done)=>{

		var minimal = new User({
			'auth': {
				'password': 'Sup3rS3cr3tP@ssw0rd123'
			},
			'email': {
				'main': 'minimal@test.com'
			},
			'name': {
				'given': 'Minimal',
				'family': 'Test'
			}
		});

		minimal.save((err, doc)=>{
			if (err || !doc){
				err = err || { 'message': 'Could not save document.' };
				return done(err);
			}

			expect(doc.auth.password).to.not.equal('Sup3rS3cr3tP@ssw0rd123');

			done();
		});
	});

	it('Should return true on model comparePass method', (done)=>{
		User.findOne({'email.main': 'minimal@test.com'}, {'auth': 1}, (err, doc)=>{
			if (err || !doc) {
				err = err || { message: 'Could not find minimal@test.com document.' };
				done(err);
			}

			User.comparePass('Sup3rS3cr3tP@ssw0rd123', doc.auth.password, (err, match)=>{
				if (err) {
					done(err);
				}

				expect(match).to.deep.equal(true);
				done();
			});
		});
	});

	it('Should return error for invaid email input', (done)=>{
		var minimal = new User({
			'auth': {
				'password': 'Sup3rS3cr3tP@ssw0rd123'
			},
			'email': {
				'main': 'www.email.domain.com'
			},
			'name': {
				'given': 'Minimal',
				'family': 'Test'
			}
		});


		minimal.save((err, doc)=>{
			expect(err.message).to.equal('User validation failed');
			expect(doc).not;
			expect(err.errors['email.main'].message).to.equal('INVALID_EMAIL_ERROR');
			done();
		});
	});
});