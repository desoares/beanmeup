var expect = require('chai').expect,
	slugify = require('../../lib/slugify'),
	options = require('../../json-data/pt-replacers');

describe('slugify - Change Uppercases to lower, spaces to "-", and special chars to pre-designed ones.', function () {
	it('Should return slugified value to "What the hell do you want"', function (done) {
		var word = 'What the hell do you want',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('what-the-hell-do-you-want');
			done();
	});

	it('Should return slugified value to "MAÇÃ"', function (done) {
		var word = 'MAÇÃ',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('macsan');
			done();
	});

	it('Should return slugified value to "maçã"', function (done) {
		var word = 'maçã',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('macsan');
			done();
	});
	
	it('Should return slugified value to "Maça"', function (done) {
		var word = 'Maça',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('macsa');
			done();
	});

	it('Should return slugified value to "MACA"', function (done) {
		var word = 'MACA',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('maca');
			done();
	});

	it('Should return slugified value to "Opnião"', function (done) {
		var word = 'Opnião',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('opniano');
			done();
	});

	it('Should return slugified value to HERE, we go -- wait', function (done) {
		var word = 'HERE, we go -- wait',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('here-we-go-wait');
			done();
	});

	it('Should return slugified value to "Onomatopéia, junções, afíns, mas à merda com isso, afins não tem acento, pô!"', function (done) {
		var word = 'Onomatopéia, junções, afíns, mas à merda com isso, afins não tem acento, pô!',
			slug = slugify(word, options);

			expect(slug).to.deep.equal('onomatopehia-juncsooes-afihns-mas-aa-merda-com-isso-afins-nano-tem-acento-pho');
			done();
	});

	it('Should return the slugified value to "Ângulos proveem uma máxima, última e, vovó, vovô ou sej o que for... Três vezes"', function (done) {
		var string = 'Ângulos proveem uma máxima, última e, vovó, vovô ou sej o que for... Três vezes',
			slug = slugify(string, options);

		expect(slug).to.deep.equal('hangulos-proveem-uma-mahxima-uhltima-e-vovoh-vovho-ou-sej-o-que-for-trhes-vezes');

		done();
	});

	it('Shoult slugify no portuguese characters "ìüäÜñ"', function (done) {
		var string = 'ìüäÜñ',
			slug = slugify(string);

		expect(slug).to.deep.equal('iuaun');
		done();
	});
});