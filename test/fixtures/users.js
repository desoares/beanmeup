module.exports.User = {
	"user0": {
		"__v" : 0,
		"_id" : "556268258b7eefdd2d2adfe2",
		'auth': {
			'password': '1234'
		},
		'email': {
			'main': 'test@test.com',
		},
		'name': {
			'given': 'Test',
			'middle': null,
			'family': 'Testerson' 
		},
		'address': {
			'home': {
				'street': 'Frist Str.',
				'number': '1',
				'neighborhood': 'Vila',
				'zipCode': '00000-000',
				'state': 'AA',
				'country': 'Brasil',
				'lat': null,
				'lon': null
			},
			'work': {
				'street': 'Frist Str.',
				'number': '1',
				'neighborhood': 'Vila',
				'zipCode': '00000-000',
				'state': 'AA',
				'country': 'Brasil',
				'lat': null,
				'lon': null
			}
		},
		'phones': [],
		'contacts': []
	},
	"user1": {//11111111111111111111111111111111111111111111111
		"__v" : 0,
		"_id" : "55641589cf4b43eb5be68812",
		'auth': {
		'password': '1234'
		},
		'email': {
			'main': 'test1@test.com',
		},
		'name': {
			'given': 'Teste',
			'middle': null,
			'family': 'O\'Test' 
		},
		'address': {
			'home': {
				'street': 'Frist Str.',
				'number': '1',
				'neighborhood': 'Vila',
				'zipCode': '00000-000',
				'state': 'AA',
				'country': 'Brasil',
				'lat': null,
				'lon': null
			},
			'work': {
				'street': 'Frist Str.',
				'number': '1',
				'neighborhood': 'Vila',
				'zipCode': '00000-000',
				'state': 'AA',
				'country': 'Brasil',
				'lat': null,
				'lon': null
			}
		},
		'phones': [],
		'contacts': []
	}//,
	// "user2": {//22222222222222222222222222222222222222222222222
	// 	"__v" : 0,
	// 	"_id" : "556538d485630fb03481c81c"
	// },
	// "user3": {//33333333333333333333333333333333333333333333333
	// 	"__v" : 0,
	// 	"_id" : "556022da561bcca642df7612"
	// }
};