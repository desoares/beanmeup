/**
 * Object prototype alteration
 * allows to pritn file line across the appliacation
 **/
var stackObj = {},
	fileObj = {},
	lineObj = {};

stackObj.get = function () {
	var origin = Error.prepareStackTrace,
		err = new Error();

	Error.prepareStackTrace = function (err, stack) {
		return stack;
	};

	Error.captureStackTrace(err, arguments.callee);

	return err.stack;
};

lineObj.get = function () {
	return __stackTraceUnity[1].getLineNumber();
};

fileObj.get = function () {
	return __stackTraceUnity[1].getFileName();
};

Object.defineProperty(global, '__stackTraceUnity', stackObj);
Object.defineProperty(global, '__file', fileObj);
Object.defineProperty(global, '__line', lineObj);