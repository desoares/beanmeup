var makeName = {
	family: function (user) {
		var name = user.name;

		return name.given + '.' + name.family;
	},
	full: function (user) {
		var name = user.name;
		if (name.middle){

			return name.given + '.' + name.middle + '.' + name.family;
		}

		return this.family(user);
	},
	reverseFam: function (user) {
		var name = user.name;
		return name.family + '.' + name.given;
	},
	reverseFull: function (user) {
		var name = user.name;

		if (name.middle) {
			return name.family + '.' + name.middle + '.' + name.given;
		}

		return this.reverseFam(user);
	},
	email: function (user) {
		var part = parseEmail(user.email.main);

		return part + '.' + user.name.family;
	},
	reverseMail: function (user) {
		var part = parseEmail(user.email.main);

		return user.name.family + '.' + part;
	},
	increment: function (user, last) {
	//this method must receive the last user with this name as argument
		var numIndex = last.split(/[a-zA-Z]+/),
			index = parseInt(numIndex[1]) || 0;

		index++;

		return user.name.given + index;
	},
	giveName: function (user){
		return;
	}
};

function parseEmail (email) {
	var atIndex = email.indexOf('@'),
		part = email.slice(0, atIndex);

	return part;
}

module.exports = makeName;