module.exports = function (name, options) {
    options = options || {};

    var aacute = options['á'] || 'a',
        atilde = options['ã'] || 'a',
        acirc = options['â'] || 'a',
        agrave = options['à'] || 'a',
        ccedil = options['ç'] || 'c',
        eacute = options['é'] || 'e',
        ecirc = options['ê'] || 'e',
        iacute = options['í'] || 'i',
        oacute = options['ó'] || 'o',
        ocirc = options['ô'] || 'o',
        otilde = options['õ'] || 'o',
        uacute = options['ú'] || 'u';

    name = name.toString()
    .replace(/\s+/g, '-')  // Replace spaces with -
    .replace(/á/gi, aacute)
    .replace(/â/gi, acirc)
    .replace(/à/gi, agrave)
    .replace(/ã/gi, atilde)
    .replace(/ä/gi, 'a')
    .replace(/ç/gi, ccedil)
    .replace(/é/gi, eacute)
    .replace(/ê/gi, ecirc)
    .replace(/ó/gi, oacute)
    .replace(/ô/gi, ocirc)
    .replace(/õ/gi, otilde)
    .replace(/í/gi, iacute)
    .replace(/ú/gi, uacute)
    .replace(/[èëẽ]/gi, 'e')
    .replace(/[ìîïĩ]/gi, 'i')
    .replace(/ñ/gi, 'n')
    .replace(/[òö]/gi, 'o')
    .replace(/[ùûũü]/gi, 'u'); // replace non english characters for english version

    if (options.keepDot) {
      name = name.replace(/[^\.^\w\-]+/g, '');  // Remove all non-word chars
    } else {
      name = name.replace(/[^\w\-]+/g, '');  // Remove all non-word chars
    }

    name = name.replace(/\-\-+/g, '-') // Replace multiple - with single -
//       .replace(/^-+/, '')  // Trim - from start of text
//       .replace(/-+$/, '')  // Trim - from end of text
    .toLowerCase();

       return name;
};