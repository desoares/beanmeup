/**
 * Generic error hanler
 * @param err Object
 * @return return a JSON for frontend screen printing
 */
module.exports = function (err /*, res*/) {
	if (!err) {
		return {
			message: 'Call to error handles with no arguments.',
			line: __line
		};
	}

	return {};
};



 /*

/**
'use strict';
 	if (!err) {
 		var err ={ 'message': 'Call to erro handler with no erro object.' };
 	}

 	if (!(err instanceof Object) && (err instanceof String)) {
 		err = { 'message': err };
 	}


 	return {};
 };

 */

// module.exports = function (err, code, res) {
// 	'use strict';

// 	code = code || 500;
// 	err.message = err.message || 'Undefined error';

// 	console.info(err, 'Error: ' + err.message + ' on line ' + err.line);

// 	return res.status(code).json(err);
// };