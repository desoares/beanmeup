/* jshint esversion: 6, strict: true */

var http = require('http'),
	express = require('express'),
	app = require('./lib/boot'),
	config = require('./config');


var server  = http.createServer(app).listen(config.port, ()=>{
	'use strict';

	console.log('Server up on ' + config.port + '. On enviroment ' + config.env + ' on line ' + __line + '.');
});