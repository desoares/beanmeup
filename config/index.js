var env = process.env.NODE_ENV || 'develop',
	config = require('./env/' + env + '.json');

config.env = env;
module.exports = config;